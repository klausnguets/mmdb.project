
$(document).ready(function(){

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }


            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#startSearch").change(function() {
        readURL(this);
    });


$("#search").click(function(){


    var base64 = getBase64Image(document.getElementById("blah"));
    
    $.ajax({
        url: 'http://localhost:8089/frontRequest',
        type: 'POST',
        data: base64,
        cache: false,
        dataType: 'json',
        processData: false, 
        contentType: false, 
        success: function(data, textStatus, jqXHR)
        {
            $('#results').html('');

            var html = "<div class='card-deck'>";
            var x = 0;
            for(var i = 0; i < data.length; i++) {
                var innerHtml = "<div class='card'> <img class='card-img-top' src='"+ data[i]+"' alt='Card image cap' width='128' height='96'></div>";
                if(x == 6 ){
                    html+= "</div><br/><div class='card-deck'>" + innerHtml;
                    x =0;
                }
                else {
                    html+= innerHtml;
                }
                x++;
            }
            html+= "</div>";

            $('#results').html(html);
            //var element = document.getElementById("results");
            //element.insertAdjacentHTML( 'beforeend', html );
        }
    });
    
});

function getBase64Image(img) {
  var canvas = document.createElement('canvas');
  var context = canvas.getContext('2d');
  canvas.height = img.naturalHeight;
  canvas.width = img.naturalWidth;
  context.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL('image/jpeg');
  return dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
}

});
