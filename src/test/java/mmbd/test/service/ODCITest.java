package mmbd.test.service;

import junit.framework.TestCase;
//import mmdb.service.ODCI;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

/**
 * Created by bahodir on 5/15/2018.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ODCITest extends TestCase {
//    public ODCI odci;

    @Test
    public void testODCIIndexCreate() throws Exception {
/*        odci.ODCIIndexCreate("test.images", "carsIndex");
        assertTrue(IndexService.exists(IndexService.INDEX));*/
    }

    @Test
    public void testODCIIndexDrop() throws Exception {
/*        odci.ODCIIndexDrop("carsIndex");
        assertEquals(IndexService.getMaxDocumentNumber("image.vary.jpg"), 0);*/
    }

    @Test
    public void testODCIIndexInsert() throws Exception {
/*        int documentsCount = IndexService.getMaxDocumentNumber(IndexService.INDEX);
        GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);

        BufferedImage img = ImageIO.read(new FileInputStream(new File("test.images\\black_car.jpg")));
        Document document = globalDocumentBuilder.createDocument(img, "test.images\\black_car.jpg");
        odci.ODCIIndexInsert(IndexService.INDEX, document);
        int docCountAfterInsert = IndexService.getMaxDocumentNumber(IndexService.INDEX);
        assertEquals((documentsCount + 1), docCountAfterInsert);*/
    }

    @Test
    public void testODCIIndexDelete() throws IOException {
/*        int documentsCount = IndexService.getMaxDocumentNumber(IndexService.INDEX + "\\image.vary.jpg");
        Document image = IndexService.getDocument(230, IndexService.INDEX + "\\image.vary.jpg");

        ODCI.ODCIIndexDelete(IndexService.INDEX, image);
        int docCountAfterDelete = IndexService.getMaxDocumentNumber(IndexService.INDEX);
        assertEquals(documentsCount - 1, docCountAfterDelete);
        try {
            // IndexService.getDocument(IndexService.INDEX, 230);
            // fail("Deleted item is still in DB");
        } catch (Exception ex) {
            //System.out.println("Success, Deleted Index is in not found in DB");
        }*/
    }

    @Test
    public void testODCIIndexUpdate() throws Exception {
/*        IndexReader indexReader = LuceneUtils.openIndexReader(IndexService.INDEX);
        Document oldDoc = indexReader.document(1234);
        GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);
        BufferedImage img = ImageIO.read(new FileInputStream(new File("test.images\\red_car.jpg")));
        Document document = globalDocumentBuilder.createDocument(img, "test.images\\red_car.jpg");

        odci.ODCIIndexUpdate(IndexService.INDEX, oldDoc, document);
        LuceneUtils.closeReader(indexReader);

        IndexReader newReader = LuceneUtils.openIndexReader(IndexService.INDEX);
        Document test  = newReader.document(newReader.maxDoc() - 1);
        assertEquals(document.get(DocumentBuilder.FIELD_NAME_IDENTIFIER), test.get(DocumentBuilder.FIELD_NAME_IDENTIFIER));
        LuceneUtils.closeReader(newReader);*/
    }

    @Test
    public void testODCIIndexStart() throws Exception {
/*        IndexReader indexReader = LuceneUtils.openIndexReader(IndexService.INDEX);
        int imageNumber = (int) Math.random() * indexReader.maxDoc();

        odci.ODCIIndexStart(IndexService.INDEX, indexReader.document(imageNumber), 3);
        assertNotNull(odci.scanContext);
        assertTrue(odci.scanContext.getSimilarEntries().size() >= 3);

        LuceneUtils.closeReader(indexReader);*/
    }

    @Test
    public void testODCIIndexFetch() throws Exception {
        // assertTrue(odci.isFetchPossible(3));
    }

    @Test
    public void testODCIIndexClose() throws Exception {
/*        IndexReader indexReader = LuceneUtils.openIndexReader(IndexService.INDEX);
        int imageNumber = (int) Math.random() * indexReader.maxDoc();

        odci.ODCIIndexStart(IndexService.INDEX, indexReader.document(imageNumber), 3);
        odci.ODCIIndexClose();

        assertEquals(odci.scanContext.getCursor(), 0);
        assertTrue(odci.scanContext.getSimilarEntries().isEmpty());
        assertTrue(odci.scanContext.getResultSet().isEmpty());

        LuceneUtils.closeReader(indexReader);*/
    }
}
