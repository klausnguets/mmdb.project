package mmdb.web.api;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.URI;

public class ApiController {

    private int port;
    private com.sun.net.httpserver.HttpServer server;

    public void Start(int port) {
        try {
            this.port = port;
            server = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(port), 0);
            System.out.println("server started at " + port);
            server.createContext("/index", new HttpHandler(){
                @Override
                public void handle(HttpExchange hE) throws IOException {
                    URI requestedUri = hE.getRequestURI();
                    String[] queryParts = requestedUri.toString().split("/");
                    int len = queryParts.length;
                    ApiRepository apiRepository = new ApiRepository();
                    if((len-1) == 1){
                        if(hE.getRequestMethod().equals("GET") && queryParts[1].equals("index")){
//                        String results = apiRepository.ListIndexes().toJSONString();
//                        hE.sendResponseHeaders(200, results.length());
//                        OutputStream os = hE.getResponseBody();
//                        os.write(results.getBytes());
//                        os.close();
                        }
                    }
                    else if((len-1) == 2){
                        if(hE.getRequestMethod().equals("GET")){
                        }
                        else if(hE.getRequestMethod().equals("POST")){
                            String results = apiRepository.CreateIndex(queryParts[2]).toJSONString();
                            hE.sendResponseHeaders(200, results.length());
                            OutputStream os = hE.getResponseBody();
                            os.write(results.getBytes());
                            os.close();
                        }
                        else if(hE.getRequestMethod().equals("DELETE")){

                        }
                    }
                    else if((len-1) == 3){
                        if(hE.getRequestMethod().equals("GET") && queryParts[3].equals("truncate")){
                        }
                        else if(hE.getRequestMethod().equals("DELETE")){
                        }
                        else {
                            JSONObject json = new JSONObject();
                            json.put("Status Code", "404");
                            json.put("Message", "Not Found");
                            hE.sendResponseHeaders(404, json.toJSONString().length());
                            OutputStream os = hE.getResponseBody();
                            os.write(json.toJSONString().getBytes());
                            os.close();
                        }
                    }
                    else if((len-1) == 4){
                        if(hE.getRequestMethod().equals("POST")){
                            String entryName = queryParts[3];
                            entryName = entryName.replace("$", "/");
                            String results = apiRepository.Insert(queryParts[2], entryName, queryParts[4]).toJSONString();
                            hE.sendResponseHeaders(200, results.length());
                            OutputStream os = hE.getResponseBody();
                            os.write(results.getBytes());
                            os.close();
                        }
                        else if(hE.getRequestMethod().equals("GET") && queryParts[3].equals("search")){
                            String entryPath = queryParts[4];
                            entryPath = entryPath.replace("$", "/");
                            String results = apiRepository.ListSimilarEntries(queryParts[2], entryPath).toJSONString();
                            hE.sendResponseHeaders(200, results.length());
                            OutputStream os = hE.getResponseBody();
                            os.write(results.getBytes());
                            os.close();
                        }
                        else {
                            JSONObject json = new JSONObject();
                            json.put("Status Code", "404");
                            json.put("Message", "Not Found");
                            hE.sendResponseHeaders(404, json.toJSONString().length());
                            OutputStream os = hE.getResponseBody();
                            os.write(json.toJSONString().getBytes());
                            os.close();
                        }
                    }
                    else {
                        JSONObject json = new JSONObject();
                        json.put("Status Code", "404");
                        json.put("Message", "Not Found");
                        hE.sendResponseHeaders(404, json.toJSONString().length());
                        OutputStream os = hE.getResponseBody();
                        os.write(json.toJSONString().getBytes());
                        os.close();
                    }

                }
            });
            server.createContext("/", new HttpHandler(){
                @Override
                public void handle(HttpExchange hE) throws IOException {
                    JSONObject json = new JSONObject();
                    json.put("Status Code", "404");
                    json.put("Message", "Not Found");
                    hE.sendResponseHeaders(404, json.toJSONString().length());
                    OutputStream os = hE.getResponseBody();
                    os.write(json.toJSONString().getBytes());
                    os.close();
                }
            });
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Stop() {
        server.stop(0);
        System.out.println("server stopped");
    }
}
