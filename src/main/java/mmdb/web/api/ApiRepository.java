package mmdb.web.api;

import javafx.application.Application;
import mmdb.service.IndexService;
import net.semanticmetadata.lire.imageanalysis.features.global.CEDD;
import net.semanticmetadata.lire.utils.FileUtils;
import oracle.sql.BFILE;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import net.semanticmetadata.lire.builders.GlobalDocumentBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ApiRepository {

//    public JSONArray ListIndexes () {
//        JSONArray jsonArray = new JSONArray();
//        ArrayList<String> resultSet =  IndexService.listIndexes("/index");
//        for( String s : resultSet){
//            jsonArray.add(s);
//        }
//        JSONObject json = new JSONObject();
//        json.put("results", jsonArray);
//        return  jsonArray;
//    }

//    public JSONObject Exists (String indexName) {
//        boolean indexExists = IndexService.exists(indexPath + "/" + indexName);
//        JSONObject indexExistJson = new JSONObject();
//        indexExistJson.put("indexExist", indexExists);
//        JSONObject json = new JSONObject();
//        json.put("results", indexExistJson);
//        return  json;
//    }

    public JSONObject CreateIndex (String indexName) throws IOException {
        boolean isSuccess = IndexService.createIndex(indexName);
        JSONObject indexCreateJson = new JSONObject();
        indexCreateJson.put("isSuccess", isSuccess);
        JSONObject json = new JSONObject();
        json.put("results", indexCreateJson);
        return  json;
    }

//    public JSONObject DeleteIndex (String indexName) {
//        boolean isSuccess = IndexService.deleteIndex(indexPath + "/" + indexName);
//        JSONObject indexDeleteJson = new JSONObject();
//        indexDeleteJson.put("isSuccess", isSuccess);
//        JSONObject json = new JSONObject();
//        json.put("results", indexDeleteJson);
//        return  json;
//    }

//    public JSONObject TruncateIndex (String indexName) {
//        boolean isSuccess = IndexService.truncateIndex(indexPath + "//" + indexName);
//        JSONObject indexTruncateJson = new JSONObject();
//        indexTruncateJson.put("isSuccess", isSuccess);
//        JSONObject json = new JSONObject();
//        json.put("results", indexTruncateJson);
//        return  json;
//    }

    public JSONObject Insert (String indexName, String entryName, String rowId) throws IOException {
        boolean isSuccess = IndexService.insert(indexName, entryName, rowId);
        JSONObject indexInsertJson = new JSONObject();
        indexInsertJson.put("isSuccess", isSuccess);
        JSONObject json = new JSONObject();
        json.put("results", indexInsertJson);
        return  json;
    }

//    public JSONObject Remove (String indexName, String entryName) throws IOException {
//        BufferedImage img = ImageIO.read(new FileInputStream(indexPath + "/" + dataSet + "/" + entryName));
//        GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);
//        Document document = globalDocumentBuilder.createDocument(img, indexPath + "/" + dataSet + "/" +
//                entryName);
//        boolean isSuccess = IndexService.remove(indexPath + "/" + indexName, document);
//        JSONObject indexInsertJson = new JSONObject();
//        indexInsertJson.put("isSuccess", isSuccess);
//        JSONObject json = new JSONObject();
//        json.put("results", indexInsertJson);
//        return  json;
//    }

    public JSONArray ListSimilarEntries (String indexName, String entryPath) throws IOException {
        ArrayList<String> resultSet =  IndexService.listSimilarEntries2(indexName, entryPath);
        JSONArray jsonArray = new JSONArray();
        for( String s : resultSet){
            jsonArray.add(s);
        }
        return  jsonArray;
    }

    public JSONArray ReturnFrontQueryResults (String entryPath) {

        JSONArray jsonArray = new JSONArray();
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Connection connection = null;

            try {
                connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@localhost:1521:orcl12c", "sys as sysdba", "oracle");
                String query = "SELECT SIMILARITY(COLUMN1, ?) as sim, COLUMN1 FROM IMG_TABLE WHERE " +
                        "similarity(COLUMN1, ?) > 0";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, entryPath);
                preparedStatement.setString(2, entryPath);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    BFILE obj = (BFILE) rs.getObject("COLUMN1");
                    jsonArray.add(obj.getDirAlias() + obj.getName());
                }
                preparedStatement.close();
                connection.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

        return  jsonArray;
    }
}
