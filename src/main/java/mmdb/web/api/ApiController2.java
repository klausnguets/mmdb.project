package mmdb.web.api;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.json.simple.JSONObject;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;

public class ApiController2 {

    private int port;
    private com.sun.net.httpserver.HttpServer server;

    public void Start(int port) {
        try {
            this.port = port;
            server = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(port), 0);
            System.out.println("server started at " + port);

            server.createContext("/frontRequest", new HttpHandler(){
                @Override
                public void handle(HttpExchange hE) throws IOException {
                    ApiRepository apiRepository = new ApiRepository();
                    if(hE.getRequestMethod().equals("POST")){
                        StringBuilder body = new StringBuilder();
                        try (InputStreamReader reader = new InputStreamReader(hE.getRequestBody())) {
                            char[] buffer = new char[256];
                            int read;
                            while ((read = reader.read(buffer)) != -1) {
                                body.append(buffer, 0, read);
                            }
                        }
                        BufferedImage image = null;
                        File outputfile = null;
                        try {
                            byte[] imageData = Base64.decode(body.toString());
                            ByteArrayInputStream bis = new ByteArrayInputStream(imageData);
                            image = ImageIO.read(bis);
                            bis.close();
                            outputfile = new File("image.jpg");
                            ImageIO.write(image, "jpg", outputfile);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String results = apiRepository.ReturnFrontQueryResults(outputfile.getAbsolutePath()).toJSONString();
                        Headers responseHeaders=hE.getResponseHeaders();
                        responseHeaders.set("Access-Control-Allow-Origin", "*");
                        hE.sendResponseHeaders(200, results.length());
                        OutputStream os = hE.getResponseBody();
                        os.write(results.getBytes());
                        os.close();
                    }
                }
            });

            server.createContext("/", new HttpHandler(){
                @Override
                public void handle(HttpExchange hE) throws IOException {
                    JSONObject json = new JSONObject();
                    json.put("Status Code", "404");
                    json.put("Message", "Not Found");
                    hE.sendResponseHeaders(404, json.toJSONString().length());
                    OutputStream os = hE.getResponseBody();
                    os.write(json.toJSONString().getBytes());
                    os.close();
                }
            });
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Stop() {
        server.stop(0);
        System.out.println("server stopped");
    }
}
