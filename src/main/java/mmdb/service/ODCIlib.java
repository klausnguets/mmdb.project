package mmdb.service;

import oracle.CartridgeServices.ContextManager;
import oracle.CartridgeServices.CountException;
import oracle.CartridgeServices.InvalidKeyException;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleConnection;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.*;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;


public class ODCIlib implements CustomDatum, CustomDatumFactory {

    public static final String _SQL_NAME = "SYS.INDEXOBJ";
    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    protected MutableStruct _struct;

    private static int[] _sqlType = { 4 };

    static CustomDatumFactory[] _factory = new CustomDatumFactory[1];

    static final ODCIlib _odciLibFactory = new ODCIlib();


   private int curnum;

    protected ODCIlib(boolean init) {
        if (init) _struct = new MutableStruct(new Object[1], _sqlType, _factory);
    }
    public ODCIlib()
    {
        _struct=new MutableStruct(new Object[1], _sqlType, _factory);
    }
    public static CustomDatumFactory getFactory(){
        return _odciLibFactory;
    }
    @Override
    public Datum toDatum(OracleConnection oracleConnection) throws SQLException {
        return _struct.toDatum(oracleConnection, _SQL_NAME);
    }

    @Override
    public CustomDatum create(Datum datum, int i) throws SQLException {
        if (datum == null) return null;
        ODCIlib o = new ODCIlib();
        o._struct = new MutableStruct((STRUCT) datum, _sqlType, _factory);
        return o;

    }

    static HttpURLConnection connection = null;

    private static void initializeHttpCon(String targetURL, String requestMethod){
        URL url = null;
        try {
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(requestMethod);
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static java.math.BigDecimal ODCIIndexDelete(oracle.ODCI.ODCIIndexInfo ia, String rid, String oldval,
                                                       oracle.ODCI.ODCIEnv env){
        /*try{
            IndexService.deleteIndex(ia.getIndexName());

        }
        catch(Exception e){

        }*/
        return ODCIConst.ODCI_SUCCESS;
    }
    public static java.math.BigDecimal ODCIIndexInsert(oracle.ODCI.ODCIIndexInfo ia, String rid, BFILE newval,
                                                       oracle.ODCI.ODCIEnv env){
        DataOutputStream wr = null;
        try {
            String entryName = newval.getDirAlias() + newval.getName();
            entryName = entryName.replace("/", "$");
            initializeHttpCon("http://localhost:8088/index/" + ia.getIndexName() + "/" + entryName + "/" +
                    rid,
                    "POST");
            wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.close();
            connection.getInputStream();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ODCIConst.ODCI_SUCCESS;
    }

    public static java.math.BigDecimal ODCIIndexDrop(oracle.ODCI.ODCIIndexInfo ia, oracle.ODCI.ODCIEnv env){
        /*try{
            IndexService.deleteIndex(ia.getIndexName());

        }catch (Exception e){

        }*/
        return ODCIConst.ODCI_SUCCESS;
    }

    public static java.math.BigDecimal ODCIIndexCreate(oracle.ODCI.ODCIIndexInfo ia, String parms, oracle.ODCI.ODCIEnv
            env){
        DataOutputStream wr = null;
        try {
            initializeHttpCon("http://localhost:8088/index/" + ia.getIndexName(),"POST");
            wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.close();
            connection.getInputStream();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ODCIConst.ODCI_SUCCESS;
    }

    public static java.math.BigDecimal ODCIIndexUpdate(oracle.ODCI.ODCIIndexInfo ia, String rid, String oldval,
                                                       String newval){
        return ODCIConst.ODCI_SUCCESS;
    }
    public static  java.math.BigDecimal ODCIIndexStart(ODCIlib[] sctx, oracle.ODCI.ODCIIndexInfo ia,
                                                       oracle.ODCI.ODCIPredInfo pi, oracle.ODCI.ODCIQueryInfo qi,
                                                       java.math.BigDecimal strt, java.math.BigDecimal stop,
                                                       String path, oracle.ODCI.ODCIEnv env) throws IOException {

        DataOutputStream wr = null;
        try {
            
            String entryName = path.replace("/", "$");

            String urlString = "http://localhost:8088/index/" + ia.getIndexName() + "/search/" + entryName;
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
            rd.close();

            JSONParser parser = new JSONParser();
            JSONArray array = (JSONArray)parser.parse(response.toString());

            ArrayList<String> listdata = new ArrayList<String>();

            if (array != null) {
                for (int i=0;i<array.size();i++){
                    listdata.add(array.get(i).toString());
                }
            }


            int key = ContextManager.setContext(listdata);
            sctx[0]=new ODCIlib();
           sctx[0]._struct.setAttribute(0, key);



        } catch (Exception e) {


        }
        return ODCIConst.ODCI_SUCCESS;
    }

    public  java.math.BigDecimal ODCIIndexFetch(java.math.BigDecimal nrows, oracle.ODCI.ODCIRidList[] rids,
                                                      oracle.ODCI.ODCIEnv env){
       try {
           ArrayList<String> listdata=(ArrayList<String>)ContextManager.getContext((int)_struct.getAttribute(0));

           String[] rlist = new String[nrows.intValue()];
           int idx = 1;
           int counter =0;
           boolean done = false;
           String rid;

           for(int i=0; done != true; i++) {
               if (idx > nrows.intValue()) {
                   done = true;
               } else {
                   if (listdata.size() > counter) {
                       rid = listdata.get(counter);
                       rlist[i] = new String(rid);
                       counter++;
                       idx++;
                   } else {
                       rlist[i] = null;
                       done = true;
                   }
               }
           }
           rids[0] = new oracle.ODCI.ODCIRidList(rlist);
       }
        catch(Exception e) {

        }


        return ODCIConst.ODCI_SUCCESS;
    }
    public java.math.BigDecimal ODCIIndexClose(oracle.ODCI.ODCIEnv env){
        try {
            ContextManager.clearContext((int)_struct.getAttribute(0));


        }catch (Exception e){

        }
        return ODCIConst.ODCI_SUCCESS;
    }

}
