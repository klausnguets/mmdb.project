package mmdb.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.imageio.ImageIO;

import net.semanticmetadata.lire.searchers.GenericFastImageSearcher;
import net.semanticmetadata.lire.searchers.ImageSearchHits;
import net.semanticmetadata.lire.searchers.ImageSearcher;
import oracle.jdbc.OracleConnection;
import oracle.sql.BFILE;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.store.FSDirectory;

import net.semanticmetadata.lire.builders.DocumentBuilder;
import net.semanticmetadata.lire.builders.GlobalDocumentBuilder;
import net.semanticmetadata.lire.imageanalysis.features.global.CEDD;
import net.semanticmetadata.lire.imageanalysis.features.global.FCTH;
import net.semanticmetadata.lire.utils.FileUtils;
import net.semanticmetadata.lire.utils.LuceneUtils;

public class IndexService {
    public static final String INDEX = "indexes";

    public static Boolean createIndex(String indexName) throws IOException {
        boolean passed = false;
        boolean isSuccess;

        String indexPath = INDEX + File.separator + indexName;
        File theIndexDir = new File(indexPath);

        // if the directory does not exist, create it
        if (!theIndexDir.exists()) {
            theIndexDir.mkdirs();
        }


        GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);
        IndexWriterConfig conf = new IndexWriterConfig(new WhitespaceAnalyzer());
        IndexWriter iw = new IndexWriter(FSDirectory.open(Paths.get(indexPath)), conf);

        LuceneUtils.closeWriter(iw);

        isSuccess = true;
        return isSuccess;
    }

    public static boolean deleteIndex(String indexName) {
        boolean isSuccess = false;
        String indexPath = INDEX + File.separator + indexName;

        try {
            IndexWriterConfig conf = new IndexWriterConfig(new WhitespaceAnalyzer());
            IndexWriter iw = new IndexWriter(FSDirectory.open(Paths.get(indexPath)), conf);
            iw.deleteAll();
            iw.commit();
            iw.close();
            // Deleting index folder
            File indexFolder = new File(indexPath);

            if (indexFolder.isDirectory() && indexFolder.exists()) {
                org.apache.commons.io.FileUtils.forceDelete(indexFolder);
            }
            isSuccess = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static boolean truncateIndex(String indexName) {
        String indexPath = INDEX + File.separator + indexName;
        boolean isSuccess = false;
        try {
            // IndexWriter iw = LuceneUtils.createIndexWriter(directory, false, LuceneUtils.AnalyzerType.WhitespaceAnalyzer);
            IndexWriterConfig conf = new IndexWriterConfig(new WhitespaceAnalyzer());
            IndexWriter iw = new IndexWriter(FSDirectory.open(Paths.get(indexPath)), conf);
            iw.deleteDocuments(new MatchAllDocsQuery());
            iw.commit();
            iw.close();
            isSuccess = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static boolean insert(String indexName, String path, String rowId) {
        String indexPath = INDEX + File.separator + indexName;
        boolean isSuccess = false;
        GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);

        try {
            BufferedImage img = ImageIO.read(new FileInputStream(path));
            Document doc = globalDocumentBuilder.createDocument(img, rowId);
            IndexWriterConfig conf = new IndexWriterConfig(new WhitespaceAnalyzer());
            IndexWriter iw = new IndexWriter(FSDirectory.open(Paths.get(indexPath)), conf);
            iw.addDocument(doc);
            iw.commit();
            iw.close();
            isSuccess = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static boolean remove(String indexName, Document doc) {
        String indexPath = INDEX + File.separator + indexName;
        boolean isSuccess = false;
        String path = doc.get(DocumentBuilder.FIELD_NAME_IDENTIFIER);
        Term term = new Term(DocumentBuilder.FIELD_NAME_IDENTIFIER, path);
        try {

            IndexWriterConfig conf = new IndexWriterConfig(new WhitespaceAnalyzer());
            IndexWriter iw = new IndexWriter(FSDirectory.open(Paths.get(indexPath)), conf);
            iw.deleteDocuments(term);
            iw.commit();
            iw.close();
            isSuccess = true;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static boolean exists(String indexName) {
        String indexPath = INDEX + File.separator + indexName;
        boolean ret = false;
        try {
            ret = DirectoryReader.indexExists(FSDirectory.open(Paths.get(indexPath)));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ret;
    }

    public static ArrayList<String> listIndexes(String indexName) {
        String indexPath = INDEX + File.separator + indexName;
        ArrayList<String> resultSet = new ArrayList<String>();
        try {
            IndexReader reader = LuceneUtils.openIndexReader(indexPath);
            for (int i = 0; i < reader.maxDoc(); i++) {
                resultSet.add(reader.document(i).get(DocumentBuilder.FIELD_NAME_IDENTIFIER));
            }
            LuceneUtils.closeReader(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static ArrayList<String> listSimilarEntries2(String indexName, String entryPath) {

        ArrayList<String> resultOfSearch = new ArrayList<String>();
        String indexPath = INDEX + File.separator + indexName;
        try {
            //Creating document for searching
            GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class);
            BufferedImage img = ImageIO.read(new FileInputStream(entryPath));
            Document documentForSearch = globalDocumentBuilder.createDocument(img, "");

            IndexReader indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPath)));
            ImageSearcher searcher = new GenericFastImageSearcher(12, CEDD.class);
            // searching with a image file ...
            ImageSearchHits hits = searcher.search(documentForSearch, indexReader);
            // searching with a Lucene document instance ...
            for (int i = 0; i < hits.length(); i++) {
                String rowId = indexReader.document(hits.documentID(i)).getValues(DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
                resultOfSearch.add(rowId);
            }
        } catch (IOException e) {

        }
        return resultOfSearch;
    }

    public static ArrayList<String> listSimilarEntries(String indexName, Document img, int nberPrintedEntries) throws IOException {

        String indexPath = INDEX + File.separator + indexName;

        ArrayList<Document> listDoc = new ArrayList<Document>();
        ArrayList<FCTH> featurePool = new ArrayList<FCTH>();
        ArrayList<Double> distancePool = new ArrayList<Double>();
        IndexReader reader = LuceneUtils.openIndexReader(indexPath);

        FCTH fcthFeaturePivot = new FCTH();
        fcthFeaturePivot.extract(ImageIO.read(new FileInputStream(img.get(DocumentBuilder.FIELD_NAME_IDENTIFIER))));
        //we prepare our lists to avoid doing calculations in the sorting loops
        for (int i = 0; i < reader.maxDoc(); i++) {
            Document doc = reader.document(i);
            if (!doc.get(DocumentBuilder.FIELD_NAME_IDENTIFIER).equals(img.get(DocumentBuilder.FIELD_NAME_IDENTIFIER))) {
                listDoc.add(doc);
                FCTH feature = new FCTH();
                feature.extract(ImageIO.read(new FileInputStream(doc.get(DocumentBuilder.FIELD_NAME_IDENTIFIER))));
                featurePool.add(feature);
                distancePool.add(fcthFeaturePivot.getDistance(feature));
            }
        }
        LuceneUtils.closeReader(reader);
        //we populate the histogram

        double distanceI, distanceJ;
        int listSize = listDoc.size();
        //we sort according to the distance
        for (int i = 0; i < listSize; i++) {
            for (int j = i + 1; j < listSize; j++) {
                distanceI = distancePool.get(i);
                distanceJ = distancePool.get(j);


                if (distanceI > distanceJ) {
                    //permutations

                    Collections.swap(listDoc, i, j);
                    Collections.swap(featurePool, i, j);
                    Collections.swap(distancePool, i, j);
                }
            }
        }
        //printing results
        ArrayList<String> resultSet = new ArrayList<String>();
        for (int i = 0; i < nberPrintedEntries; i++) {
            resultSet.add(listDoc.get(i).get(DocumentBuilder.FIELD_NAME_IDENTIFIER));
        }
        return resultSet;
    }


    public static Document getDocument(int docId, String indexName) throws IOException {
        String indexPath = INDEX + File.separator + indexName;
        IndexReader indexReader = LuceneUtils.openIndexReader(indexPath);
        Document imageDoc = indexReader.document(docId);
        LuceneUtils.closeReader(indexReader);
        return imageDoc;
    }

    public static int getMaxDocumentNumber(String indexName) throws IOException {
        String indexPath = INDEX + File.separator + indexName;
        IndexReader indexReader = LuceneUtils.openIndexReader(indexPath);
        int maxNumber = indexReader.maxDoc();
        LuceneUtils.closeReader(indexReader);
        return maxNumber;
    }


    public static void readImages() {
        try {
            File imagesFile = new File("image.vary.jpg");
            ArrayList<String> images = FileUtils.getAllImages(imagesFile, true);
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return;
            }
            Connection connection = null;
            try {
                connection = DriverManager.getConnection(
                        "jdbc:oracle:thin:@localhost:1521:orcl12c", "sys as sysdba", "oracle");
                Statement stmt = connection.createStatement();
                for (Iterator<String> it = images.iterator(); it.hasNext(); ) {
                    String path = it.next();
                    String imagesFilePath = path.substring(0, path.lastIndexOf(File.separator) + 1);
                    String imageName = path.substring(path.lastIndexOf(File.separator) + 1, path.length());
                    String query = "INSERT INTO IMG_TABLE (COLUMN1) VALUES (BFILENAME('" + imagesFilePath + "','" + imageName + "'))";
                    stmt.executeQuery(query);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


