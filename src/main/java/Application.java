import mmdb.service.IndexService;
import mmdb.service.ODCIlib;
import mmdb.web.api.*;
import net.semanticmetadata.lire.utils.LuceneUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;

public class Application {
    public static int port1 = 8088;
    public static int port2 = 8089;
    public static String dataSet = "image.vary.jpg";
    public static void main(String[] args) {



        ApiController httpServer = new ApiController();
        httpServer.Start(port1);

        ApiController2 httpServer2 = new ApiController2();
        httpServer2.Start(port2);

//            IndexService.listSimilarEntries2(IndexService.INDEX, img, 12);
//            IndexService.readImages();
    }
}
